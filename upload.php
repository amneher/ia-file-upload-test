<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOK = 1;
$fileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// check if file is an image or not
if(isset($_POST["submit"])) {
    $check = getfilesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "file is valid - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "file is not valid.";
        $uploadOk = 0;
    }
}

// check if file exists.
if(file_exists($target_file)) {
    echo "This file already exists on the server.";
    $uploadOk = 0;
}

// check file size
if($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "File size exceeds limits (50MB).";
    $uploadOk = 0;
}

// allow only certain file formats
if($fileType != "csv" && $fileType != "xls" && $fileType != "txt") {
    echo "Supported file types are: CSV, XLS, and TXT.";
    $uploadOk = 0;
}

// check if $uploadOk is set to 0 by an error
if($uploadOk == 0) {
    echo "Sorry, your file wasn't uploaded...";
} else {
    if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_name)) {
        echo "The file " . htmlspecialchars( basename( $_FILES["fileToUpload"]["name"])). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

?>
